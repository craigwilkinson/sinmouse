#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>
#include <sstream>
#include <math.h>

#define PI 3.14

int choice = 1;

std::string intToStr(int i)
{
    std::ostringstream convert;
    std::string result;
    convert << i;
    result = convert.str();

    return result;
}

int main()
{
    bool running = true;

    // check if a key is pressed later
    bool keyPressed = false;

    // set the window
    sf::RenderWindow window(sf::VideoMode(700, 100), "sinmouse!");
    window.setFramerateLimit(40);

    // load a font
    sf::Font font;
    font.loadFromFile("fonts/DejaVuSansMono.ttf");
    sf::Text text(intToStr(choice), font);
    text.setPosition(10, 10);

    // declare x and y positions
    int xPos;
    int yPos;

    // time stuff
    sf::Clock clock;
    sf::Time time;
    int t;
    int scale = 300;
    int shiftX = 1366 / 2; // screen width / 2
    int shiftY = 768 / 2; // screen width / 2

    std::string funcName;

    while (window.isOpen())
    {
        // get the time
        time = clock.getElapsedTime();
        t = time.asMilliseconds();

        // check for events
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {

            if (!keyPressed) {
                keyPressed = true;
                if (choice == 6)
                    choice = 1;
                else
                    choice++;

            }

        } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {

            if (!keyPressed) {
                keyPressed = true;
                if (choice == 1)
                    choice = 6;
                else
                    choice--;
            }

        }

        // check if we want to pause
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::F2)) {
            if (!keyPressed) {
                running = !running;
                keyPressed = true;
            }

        } else
            keyPressed = false;
        
        // only do mouse stuff if running
        if (running) {

            // check what function to use
            switch (choice) {
                case 1:
                    xPos = ceil( shiftX + scale * sin(t / 500.0) );
                    yPos = ceil( shiftY + scale * sin(t / 250.0) );
                    funcName = "sin t, sin 2t";
                    break;
                case 2:
                    xPos = ceil( shiftX + scale * sin(t / 500.0) );
                    yPos = ceil( shiftY + scale * 1 / ( 100 * sin(t/ 250.0) ) );
                    funcName = "sin t, 1/100sin 2t";
                    break;
                
                case 3:
                    xPos = ceil( shiftX + scale * sin(t / 500.0) );
                    yPos = ceil( shiftY + scale * atan(cos(t/ 250.0) ) );
                    funcName = "sin t, atan( cos( 2t ) )";
                    break;
                case 4:
                    xPos = ceil( shiftX + scale * asin(cos(t / 500.0) ) );
                    yPos = ceil( shiftY + scale * atan(sin(t/ 250.0) ) );
                    funcName = "asin( cos( t ) ), atan( sin( 2t ) )";
                    break;
                case 5:
                    xPos = ceil( shiftX + scale * (cos(t / 100.0) ) );
                    yPos = ceil( shiftY + scale * atan(sin(t/ 250.0) ) );
                    funcName = "cos t, atan( sin( t ) )";
                    break;
                case 6:
                    xPos = ceil( shiftX + scale * sin (t / 100.0) );
                    yPos = ceil( shiftY + scale * cos (t / 100.0) );
                    funcName = "sin t, cos t (circle)";
                    break;
                /*
                case 6:
                    xPos = ceil ( shiftX
                              + ( scale * ( exp ( t / 100.0) ) )
                              - ( 10 * ( sin( t / 50.0) ) ) );
                    yPos = ceil ( shiftY
                              + ( scale * ( cos( t / 100.0) ) )
                              + ( 10 * ( exp( ( t / 50.0) ) ) ) );
                    if (clock.getElapsedTime().asSeconds() > 2) {
                        clock.restart();
                    }

                    break;
                */
                default: // just incase something broke
                    xPos = 10;
                    yPos = 10;
            }    

            // set the mouse position
            sf::Mouse::setPosition(sf::Vector2i(xPos, yPos));
        }

        

        // set text number
        std::string outputStr = intToStr(choice) + ": " + funcName;
        text.setString(outputStr);
        if (running)
            text.setColor(sf::Color((int) ceil(255 * sin(t))
                                   ,(int) ceil(255 * cos(t))
                                   ,(int) ceil(255 - 255 * sin(t))));
        else
            text.setColor(sf::Color(100, 100, 100));

        // clear the window
        window.clear(sf::Color(50,50,50));

        // draw the number
        window.draw(text);

        // show the window contents
        window.display();
    }
}
